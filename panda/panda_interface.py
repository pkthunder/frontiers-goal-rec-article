"""
Classes:
    PANDAPlanRecProblem -- PANDA plan recognition problem
    PANDAPlanRecSolution -- PANDA plan recognition solution
    PANDAInterface -- Interface for PANDA
"""

import os
import re
import argparse
from dataclasses import dataclass
from typing import List

from py4j.java_gateway import JavaGateway

import numpy as np

@dataclass
class PANDAPlanRecProblem:
    """Structures for PANDA Plan recognition problem
    
    Args:
        panda_problem_file (str): PANDA Problem file
        obs_sequences (list[str]): Observation sequence
    
    Attributes:
        hddl_str (str): String representation of problem
        observations (list[str]): Observation sequence
    """

    hddl_str : str = None
    observations : list = None

    def __init__(self, panda_problem_file: str, obs_sequence: List[str],
                 domain_name: str):
        self.panda_problem_file = panda_problem_file
        with open(panda_problem_file, 'r') as f:
            self.hddl_str = "".join(f.readlines())
        self.domain_name = domain_name
        if self.domain_name == "minecraft":
            self.hddl_str = re.sub(r'(?P<name1>\w)-(?P<name2>\w)', r'\g<1>_\g<2>', self.hddl_str)
        self.observations = obs_sequence
        # pattern=r'\([\s\w-]+\)'  # Matches all whitespace and alphanumeric words in parenthesis
        # pattern_obj = re.compile(pattern)
        # self.observations = re.findall(pattern_obj, obs_sequence)

@dataclass
class PANDAPlanRecSolution:
    """Structures for PANDA Plan recognition solution
    
    Attributes:
        future_actions (list[str]): Sequence of future actions
        top_level_tasks_recognized (list[str]): List of recognized top-level tasks
    """

    future_actions : list = None
    top_level_tasks_recognized : list = None

    def __init__(self):
        self.future_actions = list()
        self.top_level_tasks_recognized = list()

class PANDAInterface:

    def __init__(self):
        self.panda_gateway = JavaGateway()
        # self.task_labels = ["obtain_pumpkin_pie", "obtain_bread",
        #                     "obtain_chicken_meat", "obtain_beef", "obtain_potato"]


    def generate(self):
        """Runs PANDA planning"""
        pass

    def recognize(self, panda_plan_rec_problem: PANDAPlanRecProblem):
        """Runs PANDA Plan Recognition
        
        Args:
            panda_plan_rec_problem (PANDAPlanRecProblem): PANDA Plan recognition
        problem

        Returns:
            plan_rec_solution (PANDAPlanRecSolution): PANDA Plan recognition
        solution
        """

        hddl_str = panda_plan_rec_problem.hddl_str

        java_observed_actions_list = self.panda_gateway.jvm.java.util.ArrayList()

        for action_obs in panda_plan_rec_problem.observations:
            java_observed_actions_list.append(action_obs)

        goal_predicted_actions_pair = \
            self.panda_gateway.entry_point.getGoal(hddl_str, " ".join(java_observed_actions_list))

        if goal_predicted_actions_pair == "ERROR":
            return goal_predicted_actions_pair

        goal, predicted_actions = goal_predicted_actions_pair.split("-")
        plan_rec_solution = PANDAPlanRecSolution()
        plan_rec_solution.top_level_tasks_recognized = goal.split(" ")
        # if plan_rec_solution.top_level_tasks_recognized[0] == "none()":
        #     index = np.random.randint(0, len(self.task_labels))
        #     plan_rec_solution.top_level_tasks_recognized = ["({})".format(self.task_labels[index])]
        # else:
        if panda_plan_rec_problem.domain_name == "minecraft":
            plan_rec_solution.top_level_tasks_recognized = \
                ["({})".format(goal[0:goal.find("(")]) for g in plan_rec_solution.top_level_tasks_recognized]
        else:
            plan_rec_solution.top_level_tasks_recognized = \
                ["({})".format(goal[0:goal.find("(")].replace("_", "-")) for g in plan_rec_solution.top_level_tasks_recognized]
        plan_rec_solution.future_actions = predicted_actions.split(" ")
        return plan_rec_solution
