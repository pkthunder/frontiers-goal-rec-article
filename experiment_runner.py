"""Goal recognition experiment runner"""

import argparse
import json
from typing import List
from collections import OrderedDict

import pandas           # Data Analysis Library
import numpy as np

from panda.panda_interface import PANDAInterface, PANDAPlanRecProblem
from elexir.elexir_interface import ElexirInterface

# -------------------------- Noise models --------------------------
def add_missing_noise(observations: list, generator: np.random.BitGenerator, noise: float):
    """Add noise to the observations

    Notes:
        We focus on systematically removing actions to assess sensitivity

    Args:
        observations (list[str]): Observations
        noise (float, default=0.0): Noise percentage between 0 and 1

    Returns:
        noisy_observations (list[str]): Noisy observations
    """

    noisy_observations = list()
    for act in observations:
        prob = generator.uniform(0.0, 1.0)
        # prob = np.random.uniform(0.0, 1.0)
        if prob > noise:
            # Keep the action
            noisy_observations.append(act)

    return noisy_observations

def add_incorrect_noise(observations: list, generator: np.random.BitGenerator,
                        noise_objects: list, noise: float):
    """Add incorrect noise to the observations

    Notes:
        We focus on systematically altering parameters to assess sensitivity

    Args:
        observations (list[str]): Observations
        noise_objects (list[str]): Object constants in a domain
        noise (float, default=0.0): Noise percentage between 0 and 1

    Returns:
        incorrect_observations (list[str]): Incorrect observations
    """

    incorrect_observations = list()
    for act in observations:
        parameters_ = act[act.find("(")+1:act.find(")")].split(",")
        parameters = list()
        for param in parameters_:
            prob = generator.uniform(0.0, 1.0)
            if prob > noise:
                # Keep the parameter
                parameters.append(param)
            else:
                obj = generator.choice(noise_objects)
                parameters.append(obj)
        incorrect_act = "{}({})".format(act[0:act.find("(")], ",".join(parameters))
        incorrect_observations.append(incorrect_act)

    return incorrect_observations
# ------------------------------------------------------------------

def read_problem_json(filename: str):
    """Read plan/goal recognition problem from JSON file

    Args:
        filename (str): JSON problem file

    Returns:
        init_state (list[str]): Initial state
        observations (list[str]): Observations
        objects (set): Objects in problem
        goal (str): Goal to recognize
    """

    json_data = None
    with open(filename, 'r') as f:
        json_data = json.load(f)

    assert "observations" in json_data and "newGoal" in json_data

    observations = list()
    for obs in json_data["observations"]:
        name = obs["pred"]
        parameters = [param["value"] for param in obs["parameters"]]
        parameters = ",".join(parameters)
        observations.append(f"{name}({parameters})")

    init_state = list()
    if "initialState" in json_data:
        for pred in json_data["initialState"]:
            name = pred["pred"]
            parameters = ",".join(pred["parameters"])
            init_state.append(f"{name}({parameters})")

    objects = list()
    if "objects" in json_data:
        for obj in json_data["objects"]:
            if (obj["object"], obj["type"]) not in objects:
                objects.append((obj["object"], obj["type"]))
    return init_state, observations, objects, json_data["newGoal"]

def to_json_obs_sequence(obs_sequence: list):
    """Create JSON observation sequence

    Notes:
        Observations have the following schema:
            {
                `pred`: str,
                `parameters`: [
                    {
                        `dfeature`: int,
                        `value`: str,
                        `type`: str
                    }
                ]
            }

    Args:
        obs_sequence (list[str]): Observation sequence

    Returns:
        json_obs_sequence (list[dict[str, obj]]): JSON observation sequence
    """
    json_obs_sequence = list()
    for _obs in obs_sequence:
        name = _obs[0:_obs.find("(")]
        params = _obs[_obs.find("(")+1:_obs.find(")")].split(",")
        parameters = [{"dfeature": -1, "value": param, "type": ""} for param in params]
        obs = {"pred": name, "parameters": parameters}
        json_obs_sequence.append(obs)
    return json_obs_sequence

def convert_to_hddl(obs_sequence: List[str]):
    """Converts obs from ELX to HDDL

    Args:
        obs_sequence (list[str]): Sequence of observations
    in ELX format

    Returns:
        new_obs_sequence (list[str]): Sequence of observations
    in HDDL format
    """

    new_obs_sequence = list()
    for obs in obs_sequence:
        _obs = obs.replace(" ", "")
        act_name = _obs[0:_obs.find("(")]
        parameters = _obs[_obs.find("(")+1:_obs.find(")")].split(",")
        new_obs = "({} {})".format(act_name, " ".join(parameters))
        # print("Old observation: {}, New Observation: {}".format(obs, new_obs))
        new_obs_sequence.append(new_obs)
    return new_obs_sequence

def save_noise(noise_traces: dict, filename: str):
    """Save original + noisy sequence

    Args:
        noise_traces (dict[str, dict[str, list[str]]]): Noise
    data
        filename (str): Noise data save file
    """
    noise_json_data = dict()
    for prob_filename, noise_trace in noise_traces.items():
        noise_json_data[prob_filename] = {
            "original": to_json_obs_sequence(noise_trace['original']),
            "noise": to_json_obs_sequence(noise_trace['noise'])
        }
    with open(filename, 'w') as f:
        json.dump(noise_json_data, f, indent=4)

def save_dataset_metrics(dataset_metrics: dict, filename: str):
    """Save dataset metrics"""

    with open(filename, 'w') as f:
        json.dump(dataset_metrics, f, indent=4)

class GoalRecExperimentRunner:
    """Goal recognition experiment runner

    Args:
        dataset_path (str): Goal recognition problem dataset path
        panda_problem_file_path (str): PANDA problem path
        elx_domain_file (str): Elexir domain file
        problem_map_file (str): Problem map file. Contains mapping between
    goal recognition problem to HDDL (JSON)
        problem_files (str): File containing list of problem files
        noise_objects_file (str): Noise object file (JSON)
        seed (int): random generator seed

    Attributes:
        dataset_path (str): Goal recognition problem dataset path
        panda_problem_file_path (str): PANDA problem path
        problem_files (list[str]) List of problem files
        interface (dict[str, Interface]): Mapping between algorithm
    and interface
        columns (list[str]): Column names for outputted CSV file
        problem_map (dict[str, str]): Problem map. Contains mapping between
    goal recognition problem to HDDL
        generator (np.random.BitGenerator): Experiment runner random generator
        random_rec_generator (np.random.BitGenerator): Random recognition random
    generator
    """

    def __init__(self, dataset_path: str, panda_problem_file_path: str,
                 elx_domain_file: str, problem_map_file: str, problem_files: str,
                 noise_objects_file: str, seed: int):
        self.dataset_path = dataset_path
        self.panda_problem_file_path = panda_problem_file_path

        with open(problem_files, 'r') as f:
            self.problem_files = [d.strip("\n") for d in f.readlines()]

        # self.problem_files = os.listdir(dataset_path)
        # self.problem_files = [f for f in self.problem_files if ".DS_Store" not in f]

        self.generator = np.random.default_rng(seed)
        self.random_rec_generator = np.random.default_rng(seed)
        self.biased_random_rec_generator = np.random.default_rng(seed)

        panda_interface = PANDAInterface()
        elexir_interface = ElexirInterface(elx_domain_file, seed)

        with open(problem_map_file, 'r') as f:
            self.problem_map = json.load(f)

        self.noise_objects = None
        if noise_objects_file is not None:
            with open(noise_objects_file, 'r') as f:
                self.noise_objects = json.load(f)
                self.noise_objects = self.noise_objects["objects"]

        self.interface = {"Elexir": elexir_interface, "PANDA": panda_interface}
        self.columns = ["Filename", "Expected Task", "Elexir Actual Task",
                        "PANDA Actual Task", "Uniform Task", "Biased Task",
                        "Elexir Correct",
                        "PANDA Correct"]

    def run_algorithms(self, init_state: List[str],
                       obs_sequence: List[str],
                       objects,
                       task_labels: List[str],
                       panda_prob_file: str):
        """Run plan/goal recognition algorithms

        Args:
            init_state (list[str]): Initial state
            obs_sequence (list[str]): Observations
            objects (set): Objects in problem
            task_labels (list[str]): List of task labels
            panda_prob_file (str): PANDA problem file

        Returns:
            panda_actual_task (str): Recognized task by PANDA
            elexir_actual_task (str): Recognzied task by Elexir
        """

        new_obs_sequence = convert_to_hddl(obs_sequence)
        problem_def = PANDAPlanRecProblem(panda_prob_file, new_obs_sequence)
        solution = \
            self.interface["PANDA"].recognize(problem_def) # type: PANDAPlanRecognitionSolution
        panda_actual_task = solution.top_level_tasks_recognized[0]

        is_successful = \
            self.interface["Elexir"].add_problem_data(
                init_state, obs_sequence, objects, task_labels)
        if is_successful:
            self.interface["Elexir"].recognize()
            queries = dict(self.interface["Elexir"].elex_obj.rec.query)
            _queries = dict()
            for _tlt_atom,  query_results in queries.items():
                _tlt = _tlt_atom.getString()
                _queries[_tlt] = query_results
            queries = _queries
            elexir_actual_task = \
                self.interface["Elexir"].get_highest_prob_task(task_labels, queries)
            elexir_actual_task = "({})".format(elexir_actual_task)
        else:
            elexir_actual_task = "(error)"
        self.interface["Elexir"].reset()
        return panda_actual_task, elexir_actual_task

    def get_task_distribution(self, task_labels):
        """Get task distribution

        Args:
            task_labels (list[str]): List of task labels

        Returns:
            task_dist (list[float]): Task distribution
        """
        task_dist_ = OrderedDict()
        for task in task_labels:
            task_dist_[task] = 0

        for elx_file in self.problem_files:
            _, _, _, expected_task = \
                read_problem_json(self.dataset_path + "/" + elx_file)
            task_dist_[expected_task] += 1
        total = sum([val for _, val in task_dist_.items()])
        task_dist = list()
        for task in task_labels:
            task_dist.append(task_dist_[task]/total)
        return task_dist

    def execute_experiment(self, output_file: str, noise_output_file: str,
                           task_labels, noise_type: str, noise):
        """Execute goal recognition experiment

        Args:
            output_file (str): Output CSV file
            noise_output_file (str): Output noise file (can be None)
            task_labels (list[str]): List of task labels
            noise_type (str): Type of noise (can be None)
            noise (float): Noise percentage between 0 and 1
        """

        task_dist = self.get_task_distribution(task_labels)

        expected_task_to_avg_length = dict()
        for task in task_labels:
            expected_task_to_avg_length[task] = {"total": 0, "num": 0}

        print("======================== Starting Experiments ========================")

        experiment_result_table = list()
        noise_traces = OrderedDict()

        for i, elx_file in enumerate(self.problem_files):
            panda_prob_file = self.problem_map[elx_file]
            panda_prob_file = f"{self.panda_problem_file_path}/{panda_prob_file}"

            print(f"i: {i}, Elexir file: {elx_file}, PANDA file: {panda_prob_file}")

            init_state, obs_sequence, objects, expected_task = \
                read_problem_json(self.dataset_path + "/" + elx_file)

            if noise_output_file is not None:
                assert noise_type is not None and noise > 0.00000000000000000000001

                if noise_type == "missing":
                    noise_obs_sequence = add_missing_noise(obs_sequence, self.generator, noise)
                elif noise_type == "incorrect":
                    assert self.noise_objects is not None
                    _objects = [obj for obj, _ in objects]
                    noise_objects = self.noise_objects[:]
                    for obj in _objects:
                        if obj not in noise_objects:
                            noise_objects.append(obj)
                    # print("Noise objects: ", noise_objects)
                    # noise_objects = list(set(self.noise_objects + _objects))
                    noise_obs_sequence = add_incorrect_noise(obs_sequence, self.generator,
                                                             noise_objects, noise)
                else:
                    raise ValueError(f"Incorrect noise type provided: {noise_type}")

                noise_traces[elx_file] = {
                    "original": obs_sequence,
                    "noise": noise_obs_sequence
                    }
                obs_sequence = noise_obs_sequence

            expected_task_to_avg_length[expected_task]["total"] += len(obs_sequence)
            expected_task_to_avg_length[expected_task]["num"] += 1
            expected_task = "({})".format(expected_task)

            panda_actual_task = "(none)"
            elexir_actual_task = "(none)"
            if len(obs_sequence) != 0:
                panda_actual_task, elexir_actual_task = \
                    self.run_algorithms(
                        init_state, obs_sequence, objects, task_labels,
                        panda_prob_file)
            else:
                print("Observation sequence is empty!")

            # random_task = "(none)"
            uniform_index = self.random_rec_generator.integers(0, len(task_labels))
            biased_index = self.biased_random_rec_generator.choice(
                range(len(task_labels)), p=task_dist)
            # index = np.random.randint(0, len(task_labels))
            uniform_task = "({})".format(task_labels[uniform_index])
            biased_task = "({})".format(task_labels[biased_index])

            print("Expected task: {}, Actual task from PANDA: {}, "\
                "Actual task from Elexir: {}".format(
                    expected_task, panda_actual_task, elexir_actual_task))

            experiment_result_table.append(
                [elx_file, expected_task, elexir_actual_task, panda_actual_task,
                 uniform_task, biased_task,
                 int(expected_task == elexir_actual_task),
                 int(expected_task == panda_actual_task)])
            print("===============================================================================")
        # for task, stats in expected_task_to_avg_length.items():
        #     print("Task: {}, Average: {}".format(task, stats["total"]/stats["num"]))
        if noise_output_file is not None:
            save_noise(noise_traces, noise_output_file)
        save_dataset_metrics(expected_task_to_avg_length, "dataset-metrics.json")

        experiment_results = pandas.DataFrame(experiment_result_table, columns=self.columns)
        experiment_results.to_csv(output_file)

def main():
    """Main function"""

    parser = argparse.ArgumentParser("Goal Recognition Experiment Runner")
    parser.add_argument("config",
                        type=str,
                        help="Experiment configuration file (JSON)")

    args = parser.parse_args()
    config_file = args.config
    with open(config_file, 'r') as f:
        json_data = json.load(f)

    dataset_path = json_data.get("dataset_path", None)
    panda_prob_path = json_data.get("panda_problem_path", None)
    output_file = json_data.get("output_filename", None)
    elx_domain_file = json_data.get("elx_domain_file", None)
    problem_map = json_data.get("problem_map", None)
    problem_files = json_data.get("problem_files", None)
    task_labels = json_data.get("task_labels", None)
    rng_seed = json_data.get("rng_seed", 1)

    noise = json_data.get("noise", 0.0)
    noise_type = json_data.get("noise_type", None)
    noise_objects_file = json_data.get("noise_objects_file", None)
    noise_output_file = json_data.get("noise_output_file", None)

    if dataset_path is None:
        raise ValueError("Dataset path is missing!")
    if panda_prob_path is None:
        raise ValueError("PANDA problem path is missing!")
    if output_file is None:
        raise ValueError("Output file is missing!")
    if elx_domain_file is None:
        raise ValueError("Elexir domain file is missing!")
    if problem_map is None:
        raise ValueError("Problem mapping between Elexir and PANDA is missing!")
    if problem_files is None:
        raise ValueError("File containing problem files are missing!")
    if task_labels is None:
        raise ValueError("Task labels are missing!")

    experiment_runner = GoalRecExperimentRunner(
        dataset_path, panda_prob_path, elx_domain_file, problem_map, problem_files,
        noise_objects_file, rng_seed)
    experiment_runner.execute_experiment(
        output_file, noise_output_file, task_labels, noise_type, noise)

if __name__ == "__main__":
    main()
