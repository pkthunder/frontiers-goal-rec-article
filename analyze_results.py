"""Experiment analyzer"""

from typing import List
import argparse
import json

# import numpy
import pandas
import seaborn as sns   # Data visualization tool
import matplotlib.pyplot as plt

from sklearn.metrics import precision_recall_fscore_support

# Initializes Seaborn
sns.set(font_scale=1.5)
# sns.set_palette("dark", 5)
# sns.set_palette(sns.color_palette("colorblind")[0:4] + [sns.color_palette("colorblind")[5]])

OUTPUT_DIR = "journal-results/03-10-2021/results-06-28-2021"
# ROUND_FUNCTION = lambda x: round(x, 3)

# SIGNIFICANCE_LEVEL = 0.01
TASKS_TO_IGNORE = ("(none)", "(timeout)", "(error)")

def plot_inspectibility_results(dataframe: pandas.DataFrame, filename: str):
    """Plot inspectibility results

    Args:
        dataframe (pandas.DataFrame): Precision, Recall, F1-Score metrics
        filename (str): Output filename
    """

    _dataframe = dataframe.loc[~dataframe["Label"].str.fullmatch("All")]
    _dataframe = _dataframe.loc[_dataframe["Noise"].str.fullmatch("0.0")]

    plot_dimensions = (30, 10)
    fig = plt.figure(figsize=plot_dimensions)
    ax = fig.add_subplot(111)
    sns.barplot(
            x=_dataframe["Label"], y=_dataframe["F1 Score"],
            data=_dataframe,
            hue="Algorithm", ax=ax)
    ax.set(xlabel="Top-level Goals")
    ax.legend(loc='upper center', bbox_to_anchor=(0.60, 1.16), ncol=3)
    ax.set_ylim(0.0, 1.0)
    ax.figure.savefig(f"{OUTPUT_DIR}/{filename}")
    plt.close()

def plot_reliability_results(dataframe: pandas.DataFrame, task_labels: List[str], filename: str,
                             plot_task_graphs=False):
    """Plot reliability results

    Args:
        dataframe (pandas.DataFrame): Precision, Recall, F1-Score metrics
        task_labels (list[str]): List of top-level task labels
        filename (str): Output filename
    """

    plot_dimensions = (10, 7)
    if plot_task_graphs:
        for task_label in task_labels:
            if task_label in TASKS_TO_IGNORE:
                continue

            _task_label = task_label[task_label.find("(")+1:task_label.find(")")]
            _task_label = _task_label.replace("_", " ")
            _dataframe = dataframe.loc[dataframe["Label"].str.fullmatch(_task_label)]

            fig = plt.figure(figsize=plot_dimensions)
            ax = fig.add_subplot(111)
            sns.lineplot(
                x=_dataframe["Noise"], y=_dataframe["F1 Score"],
                data=_dataframe,
                hue="Algorithm", ax=ax)
            _task_label = _task_label.replace(" ", "-")
            ax.legend(loc='upper center', bbox_to_anchor=(0.60, 1.16), ncol=3)
            ax.set_ylim(0.0, 1.0)
            ax.figure.savefig(f"{OUTPUT_DIR}/{_task_label}-{filename}")
            plt.close()

    _dataframe = dataframe.loc[dataframe["Label"].str.fullmatch("All")]
    _dataframe = _dataframe.astype({"Noise": float})
    _dataframe["Noise"]*=100
    _dataframe = _dataframe.astype({"Noise": str})

    fig = plt.figure(figsize=plot_dimensions)
    ax = fig.add_subplot(111)
    sns.lineplot(
        x=_dataframe["Noise"], y=_dataframe["F1 Score"],
        data=_dataframe,
        hue="Algorithm", ax=ax)
    ax.legend(loc='upper center', bbox_to_anchor=(0.60, 1.16), ncol=3)
    ax.set(xlabel="Noise Percentage")
    ax.set_ylim(0.0, 1.0)
    ax.figure.savefig(f"{OUTPUT_DIR}/all-{filename}")
    plt.close()

def create_conf_matrices(dataframe: pandas.DataFrame,
                         task_labels: List[str],
                         elexir_filename: str,
                         panda_filename: str,
                         rager_filename: str):
    """Create confusion matrices

    Args:
        dataframe (pandas.DataFrame): experiment results
        task_labels (list[str]): List of top-level task labels
        elexir_filename (str): Elexir output filename
        panda_filename (str): PANDA output filename
    """

    print("Constructing confusion matrix for Elexir and PANDA...")
    alg_to_filename = {"Elexir": elexir_filename, "PANDA": panda_filename,
                       "RAGeR": rager_filename}

    for alg, filename in alg_to_filename.items():
        conf_matrix_table = list()

        for expected_task in task_labels:
            expected_task_elements = \
                dataframe[dataframe["Expected Task"].str.contains(expected_task)]
            for actual_task in task_labels:
                frequency = len(expected_task_elements[expected_task_elements[
                    f"{alg} Actual Task"].str.contains(actual_task)])

                _expected_task = expected_task[expected_task.find("(")+1:expected_task.find(")")]
                _expected_task = _expected_task.replace("_", "\n")

                _actual_task = actual_task[actual_task.find("(") + 1:actual_task.find(")")]
                _actual_task = _actual_task.replace("_", "\n")

                row = [_expected_task, _actual_task, frequency]
                conf_matrix_table.append(row)

        columns = ["Ground Truth", "Predicted", "Frequency"]
        conf_matrix = pandas.DataFrame(conf_matrix_table, columns=columns)

        result = conf_matrix.pivot(index="Ground Truth", columns="Predicted", values="Frequency")

        # plot_dimensions = (10, 10)
        # fig = plt.figure(figsize=plot_dimensions)
        # ax = fig.add_subplot(111)

        grid_kws = {"height_ratios": (.9, .05), "hspace": .15}
        _, (ax, cbar_ax) = plt.subplots(2, gridspec_kw=grid_kws, figsize=(12, 11))

        ax = sns.heatmap(result, annot=True, fmt="d", ax=ax, cmap="Blues",
                         cbar_ax=cbar_ax,
                         cbar_kws={"orientation": "horizontal","label": "Number of Instances"},
                         annot_kws={"fontsize": 35})
        ax.xaxis.set_label_position("top")
        ax.xaxis.tick_top()
        # ax = sns.heatmap(result, annot=True, fmt="d", ax=ax)
        # ax.set_title("Confusion Matrix for Goal Recognition")
        ax.figure.savefig(f"{OUTPUT_DIR}/{filename}")

    print("Confusion Matrix Constructed! Saving!")

def compute_alg_metrics(dataframe, ground_truth,
                        noise_level, rng_seed,
                        task_labels: List[str],
                        alg_name: str):
    """Computes metrics for a given algorithm

    Notes:
        Metrics computed include Precision, Recall, and F1 Score

    Args:
        dataframe (pandas.DataFrame): Results for a particular algorithm
        ground_truth (pandas.DataFrame): Ground truth
        noise_level (float): Noise level
        rng_seed (int): RNG seed
        task_labels (list[str]): List of top-level task labels
        alg_name (str): Algorithm name

    Returns:
        class_metrics (list[obj]): Metrics
    """

    _task_labels = task_labels[:]
    _task_labels.remove("(none)")

    precision, recall, f1_score, _ = \
        precision_recall_fscore_support(ground_truth, dataframe,
                                        labels=_task_labels)
    class_metrics = list()
    for i, label in enumerate(_task_labels):
        if label in TASKS_TO_IGNORE:
            continue

        print(f"======================== {alg_name}-{label} ========================")
        print(f"Precision: {precision[i]}, Recall: {recall[i]}, F1: {f1_score[i]}")
        _label = label[label.find("(")+1:label.find(")")].replace("_", " ")

        class_metrics.append(
            [str(noise_level), rng_seed, _label, alg_name,
            precision[i], recall[i], f1_score[i]])

    precision, recall, f1_score, _ = \
        precision_recall_fscore_support(ground_truth, dataframe,
                                        labels=_task_labels, average="macro")
    class_metrics.append(
        [str(noise_level), rng_seed, "All", alg_name,
         precision, recall, f1_score])
    return class_metrics

def compute_f1_score(dataframes: List, task_labels: List[str], filename: str):
    """Computes multiclass F1 score

    Args:
        dataframes (pandas.DataFrame): experiment results
        task_labels (list[str]): List of top-level task labels
        filename (str): output filename

    Returns:
        class_metric_dataframe (pandas.DataFrame): Dataframe containing
    class metrics
    """
    class_metrics = list()

    for method, noise_level, rng_seed, dataframe in dataframes:
        ground_truth = dataframe["Expected Task"]
        print(f"---------------------- Run: {noise_level}-{rng_seed} ----------------------")
        if method == "main":
            class_metrics += compute_alg_metrics(dataframe["Elexir Actual Task"],
                                                 ground_truth, noise_level,
                                                 rng_seed, task_labels, "Elexir-MCTS")

            class_metrics += compute_alg_metrics(dataframe["PANDA Actual Task"],
                                                 ground_truth, noise_level,
                                                 rng_seed, task_labels, "PANDA-Rec")

            if "RAGeR Actual Task" in dataframe.columns:
                class_metrics += compute_alg_metrics(dataframe["RAGeR Actual Task"],
                                                     ground_truth, noise_level,
                                                     rng_seed, task_labels, "RAGeR")
            else:
                print("RAGeR dataframe: ", dataframe)
                raise Exception(
                    f"RAGeR not available for noise {noise_level} and seed {rng_seed}")

        elif method == "random":
            class_metrics += compute_alg_metrics(dataframe["Uniform Task"],
                                                 ground_truth, noise_level,
                                                 rng_seed, task_labels, "Uniform")

            class_metrics += compute_alg_metrics(dataframe["Biased Task"],
                                                 ground_truth, noise_level,
                                                 rng_seed, task_labels, "Biased")
        else:
            raise ValueError("Undefined method: ", method)

    class_metric_dataframe = pandas.DataFrame(
        class_metrics, columns=["Noise", "Seed", "Label", "Algorithm",
                                "Precision", "Recall", "F1 Score"])
    class_metric_dataframe.to_csv(f"{OUTPUT_DIR}/{filename}")
    return class_metric_dataframe

def read_csv(filename: str):
    """Reads CSV file

    Args:
        filename (str): CSV file

    Returns:
        dataframe (pandas.DataFrame)
    """

    dataframe = pandas.read_csv(filename, index_col=0)
    dataframe = dataframe.replace("(timeout)", "(none)")
    dataframe = dataframe.replace("(error)", "(none)")
    return dataframe

def main():
    """Main function"""
    parser = argparse.ArgumentParser("Result Analyzer")
    parser.add_argument("config",
                        type=str,
                        help="Experiment configuration file (JSON)")

    args = parser.parse_args()

    config_file = args.config
    with open(config_file, 'r') as f:
        json_data = json.load(f)

    noiseless_result_file = json_data.get("noiseless_result_file", None)
    noise_result_files = json_data.get("noise_result_files", [])
    random_files = json_data.get("random_files", None)
    task_labels = json_data.get("task_labels", None)

    output_filename = json_data.get("output_filename", "exp_results.csv")
    elexir_conf_filename = json_data.get("elexir_conf_filename", "elexir_conf.png")
    panda_conf_filename = json_data.get("panda_conf_filename", "panda_conf.png")
    rager_conf_filename = json_data.get("rager_conf_filename", "rager_conf.png")
    noise_plot_filename = json_data.get("noise_plot_filename", "noise_plot.png")

    assert noise_result_files is not None
    assert random_files is not None
    assert task_labels is not None

    dataframes = list()
    noise_free_dataframe = read_csv(noiseless_result_file)
    dataframes.append(("main", 0.0, 1, noise_free_dataframe))

    for noise_result_file in noise_result_files:
        split_filename = noise_result_file.split("_")
        rng_seed = int(split_filename[-2])
        noise_level = float(split_filename[-1][0:split_filename[-1].find(".csv")])
        dataframe = read_csv(noise_result_file)
        print(f"Noise level: {noise_level}, RNG seed: {rng_seed}")
        dataframes.append(("main", noise_level, rng_seed, dataframe))

    noise_levels = [0.0] if len(noise_result_files) == 0 \
        else [0.0, 0.25, 0.50, 0.75]

    for random_file in random_files:
        split_filename = random_file.split("_")
        rng_seed = int(split_filename[-2])
        dataframe = read_csv(random_file)
        for noise_level in noise_levels:
            print(f"Noise level: {noise_level}, RNG seed: {rng_seed}")
            dataframes.append(("random", noise_level, rng_seed, dataframe))

    class_metric_dataframe = \
        compute_f1_score(dataframes, task_labels, output_filename)

    plot_inspectibility_results(class_metric_dataframe, f"{output_filename}.png")

    create_conf_matrices(noise_free_dataframe,
                         task_labels,
                         elexir_conf_filename,
                         panda_conf_filename,
                         rager_conf_filename)

    if len(noise_result_files) != 0:
        plot_reliability_results(class_metric_dataframe, task_labels, noise_plot_filename)

if __name__ == "__main__":
    main()
