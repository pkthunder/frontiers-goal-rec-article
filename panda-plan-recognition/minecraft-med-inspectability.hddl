;; PANDA Domain Model for HTN-based Plan Recognition as Planning in Minecraft

;; Written by: Pavan Kantharaju
;; Revisions:
;;  1.0 - Initial Version
;;  1.01 - Commented out unnecessary top-level tasks. Removed state model
;;  1.02 - Cleaned up PANDA model by removing commented out content


;; Current Top-Level Tasks:
;; - Obtain Beetroot - Commented out
;; - Obtain Carrots - Commented out
;; - Obtain Potatoes
;; - Obtain Bread
;; - Obtain Chicken Meat
;; - Obtain Beef
;; - Obtain Pumpkin Pie (ordering of item gathering mirrors SHOP2 model)
;; - Obtain Cake (ordering of item gathering mirrors SHOP2 model)

;; NOTES:
;; - Only certain methods and actions have preconditions.
;; - The actions here should be based on what an agent can observe.

(define (domain minecraft)


  (:types
    location_type - object_type
    farmland_type entity_type - location_type

    iron_type wood_type - material_type
    hoe_type shovel_type pickaxe_type axe_type - tool_type
    iron_sword_type - sword_type
    sword_type axe_type - weapon_type
    beef_type chicken_meat_type - meat_type
    cake_type pumpkin_pie_type carrot_type potato_type beetroot_type meat_type bread_type poisonous_potato_type - food_type
    chicken_type cow_type - animal_type
    wheat_seeds_type potato_type reed_type carrot_type beetroot_seeds_type - non_stem_seeds_type
    pumpkin_seeds_type melon_seeds_type - stem_seeds_type
    stem_seeds_type non_stem_seeds_type - seeds_type
    seeds_type egg_type feather_type meat_type leather_type block_type - drop_type

    non_stem_crop_type stem_crop_type - crop_type
    wheat_type carrot_type potato_type beetroot_type - non_stem_crop_type
    reed_type pumpkin_type - stem_crop_type

    skeleton_type - hostile_type

    sugar_type milk_bucket_type bone_meal_type - ingredient_type
    glass_type torch_type dirt_type water_type air_type stem_type pumpkin_type - block_type
    stem_type crop_type - plantable_type

    hostile_type animal_type - mob_type
    chest_type - container_type
    tool_type weapon_type food_type seeds_type drop_type crop_type ingredient_type material_type - item_type
    player_type agent_type mob_type item_type container_type block_type xporb_type - entity_type
  )

  (:constants
    air - air_type
    bread cake pumpkin_pie - food_type
    chicken_meat beef - meat_type
    crafted_iron_sword - iron_sword_type
    crafted_sword - sword_type

    ;; NOTE: These should be put into the object section of the problem file.
    pumpkin_stem - stem_type
    melon_stem - stem_type
    melon - crop_type
    pumpkin - pumpkin_type

    wheat - wheat_type
    potato - potato_type
    pumpkin_seeds - pumpkin_seeds_type
    reed - reed_type
    carrot - carrot_type
    beetroot - beetroot_type
    chicken - chicken_type
    cow - cow_type
    wheat_seeds - wheat_seeds_type
    beetroot_seeds - beetroot_seeds_type
    egg - egg_type
    milk_bucket - milk_bucket_type
    iron_sword - iron_sword_type
    sugar - sugar_type
    )

  (:predicates
    (found_near ?item - item_type ?entity - entity_type)
    (grows_from ?crop - item_type ?seed - item_type)
    )

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; --------- Tasks --------
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; ----- Necessary Tasks for Plan Recognition -----
  (:task mtlt :parameters ()) ;; use this in the initial task network to (possibly) have multiple top level tasks
  (:task tlt :parameters ())  ;; use this to have a single top level task
  ;; ------------------------------------------------

  (:task obtain_cake :parameters ())
  (:task obtain_pumpkin_pie :parameters ())
  (:task obtain_chicken_meat :parameters ())
  (:task obtain_beef :parameters ())
  (:task obtain_bread :parameters ())
  (:task obtain_potato :parameters ())

  (:task obtain_three_wheat :parameters (?wheat_1 - wheat_type ?wheat_2 - wheat_type ?wheat_3 - wheat_type))
  (:task obtain_cake_ingredients :parameters ())
  (:task obtain_pumpkin_pie_ingredients :parameters ())
  (:task obtain_sugar_for_cake :parameters ())
  (:task obtain_milk_bucket_for_cake :parameters ())
  (:task obtain_bone_meal :parameters ())
  (:task slaughter :parameters (?animal - animal_type ?meat  - meat_type))
  (:task consume_or_obtain :parameters (?item - item_type))
  (:task grow_and_harvest :parameters (?crop - crop_type))
  (:task find_farmland :parameters (?target - location_type))
  (:task grow_with_bone_meal :parameters (?seed - seeds_type ?target - location_type))
  (:task obtain_growing_items :parameters ())
  (:task select_and_use_bone_meal :parameters ())

  ;; ----- Necessary Methods for Plan Recognition -----

  (:method m-multiple-tlt
    :parameters ()
    :task (mtlt)
    :subtasks (and
      (mtlt)
      (tlt))
    )

  (:method m-del-mtlt
    :parameters ()
    :task (mtlt)
    :subtasks ()
    )

  (:method m_tlt_obtain_pumpkin_pie
    :parameters ()
    :task (tlt)
    :subtasks (and (obtain_pumpkin_pie))
    )

  (:method m_tlt_obtain_cake
    :parameters ()
    :task (tlt)
    :subtasks (and (obtain_cake))
    )

  (:method m_tlt_obtain_bread
    :parameters ()
    :task (tlt)
    :subtasks (and (obtain_bread))
    )

  (:method m_tlt_obtain_potato
    :parameters ()
    :task (tlt)
    :subtasks (and (obtain_potato))
    )

  (:method m_tlt_obtain_chicken_meat
    :parameters ()
    :task (tlt)
    :subtasks (and (obtain_chicken_meat))
    )

  (:method m_tlt_obtain_beef
    :parameters ()
    :task (tlt)
    :subtasks (and (obtain_beef))
    )

  ;; --------------------------------------------------

  ;; Planning Methods

  (:method m_obtain_pumpkin_pie_1
    ;; Craft Item
    :parameters (?pumpkin - pumpkin_type ?sugar - sugar_type ?egg - egg_type)
    :task (obtain_pumpkin_pie)
    :precondition ()
    :ordered-subtasks (and
      (obtain_pumpkin_pie_ingredients)
      ;; (craft_item pumpkin_pie)
      )
    )

  (:method m_obtain_pumpkin_pie_ingredients_1
    :parameters (?pumpkin - pumpkin_type ?sugar - sugar_type ?egg - egg_type)
    :task (obtain_pumpkin_pie_ingredients)
    :precondition ()
    :ordered-subtasks (and
      (consume_or_obtain ?pumpkin)
      (consume_or_obtain ?sugar)
      (consume_or_obtain ?egg)
      )
    )
  
  (:method m_obtain_pumpkin_pie_ingredients_2
    :parameters (?pumpkin - pumpkin_type ?sugar - sugar_type ?egg - egg_type)
    :task (obtain_pumpkin_pie_ingredients)
    :precondition ()
    :ordered-subtasks (and
      (consume_or_obtain ?pumpkin)
      (consume_or_obtain ?sugar)
      )
    )

  (:method m_obtain_pumpkin_pie_ingredients_3
    :parameters (?pumpkin - pumpkin_type ?sugar - sugar_type ?egg - egg_type)
    :task (obtain_pumpkin_pie_ingredients)
    :precondition ()
    :ordered-subtasks (and
      (consume_or_obtain ?pumpkin)
      (consume_or_obtain ?egg)
      )
    )

 (:method m_obtain_cake_1
   ;; Craft Item
   :parameters ()
   :task (obtain_cake)
   :precondition ()
   :ordered-subtasks (and
     (obtain_cake_ingredients)
     ;; (craft_item cake)
     )
   )

 (:method m_obtain_cake_ingredients_1
   :parameters (?egg_1 - egg_type)
   :task (obtain_cake_ingredients)
   :ordered-subtasks (and
     (obtain_sugar_for_cake)
     (consume_or_obtain ?egg_1)
     (obtain_milk_bucket_for_cake)
     (obtain_three_wheat)
     )
   )

 (:method m_obtain_sugar_for_cake_1
   :parameters (
     ?sugar_1 - sugar_type
     ?sugar_2 - sugar_type
     )
   :task (obtain_sugar_for_cake)
   :ordered-subtasks (and
     (consume_or_obtain ?sugar_1)
     (consume_or_obtain ?sugar_2)
     )
   )

 (:method m_obtain_milk_bucket_for_cake_1
   :parameters (
     ?bucket_1 - milk_bucket_type
     ?bucket_2 - milk_bucket_type
     ?bucket_3 - milk_bucket_type
     )
   :task (obtain_milk_bucket_for_cake)
   :ordered-subtasks (and
     (consume_or_obtain ?bucket_1)
     (consume_or_obtain ?bucket_2)
     (consume_or_obtain ?bucket_3)
     )
   )

 (:method m_obtain_three_wheat_1
   :parameters (
     ?wheat_1 -  wheat_type
     ?wheat_2 - wheat_type
     ?wheat_3 - wheat_type
     )
   :task (obtain_three_wheat ?wheat_1 ?wheat_2 ?wheat_3)
   :ordered-subtasks (and
     (consume_or_obtain ?wheat_1)
     (consume_or_obtain ?wheat_2)
     (consume_or_obtain ?wheat_3)
     )
   )

  (:method m_obtain_bread_1
    ;; Craft bread
    :parameters (?wheat_1 - wheat_type ?wheat_2 - wheat_type ?wheat_3 - wheat_type)
    :task (obtain_bread)
    :precondition ()
    :ordered-subtasks (and
      (obtain_three_wheat ?wheat_1 ?wheat_2 ?wheat_3)
      ;; (craft_item bread)
      )
    )

  (:method m_obtain_potato_1
    ;; Grow crop
    :parameters (?potato - potato_type)
    :task (obtain_potato)
    :precondition ()
    :ordered-subtasks (and (grow_and_harvest ?potato))
    )

  ;; (:method m_obtain_chicken_meat_1
  ;;   ;; Meat alraedy exists
  ;;   :parameters (?meat - chicken_meat_type ?location - location_type)
  ;;   :task (obtain_chicken_meat)
  ;;   :precondition ()
  ;;   :ordered-subtasks (and (gather ?meat))
  ;;   )

  (:method m_obtain_chicken_meat_1
    ;; Meat alraedy exists
    :parameters (?meat - chicken_meat_type)
    :task (obtain_chicken_meat)
    :precondition ()
    :ordered-subtasks (and (consume_or_obtain ?meat))
    )

  (:method m_obtain_chicken_meat_2
    ;; Meat doesn't exist. Need to kill chicken
    :parameters (?chicken - chicken_type)
    :task (obtain_chicken_meat)
    :precondition ()
    :ordered-subtasks (and (slaughter ?chicken chicken_meat))
    )

  ;; (:method m_obtain_beef_1
  ;;   ;; Meat already exists
  ;;   :parameters (?meat - beef_type ?location - location_type)
  ;;   :task (obtain_beef)
  ;;   :precondition ()
  ;;   :ordered-subtasks (and (gather ?meat))
  ;;   )

  (:method m_obtain_beef_1
    ;; Meat already exists
    :parameters (?meat - beef_type)
    :task (obtain_beef)
    :precondition ()
    :ordered-subtasks (and (consume_or_obtain ?meat))
    )

  (:method m_obtain_beef_2
    ;; Slaugher a cow
    :parameters (?cow - cow_type)
    :task (obtain_beef)
    :precondition ()
    :ordered-subtasks (and (slaughter ?cow beef))
    )

  (:method m_slaughter_1
    ;; Animal has been seen
    :parameters (?animal - animal_type ?meat - meat_type ?location - location_type)
    :task (slaughter ?animal ?meat)
    :precondition ()
    :ordered-subtasks (and
      (move ?animal)
      (look_at ?animal)
      (select iron_sword) ;; Removed ?sword for now as we only use an iron sword
      (attack ?animal)
      (gather ?meat))
    )
 
 (:method m_slaughter_2
    ;; Animal has been seen
    :parameters (?animal - animal_type ?meat - meat_type ?location - location_type)
    :task (slaughter ?animal ?meat)
    :precondition ()
    :ordered-subtasks (and
      (move ?animal)
      (look_at ?animal)
      (attack ?animal)
      (gather ?meat))
    )
  
  (:method m_slaughter_3
    ;; Animal has been seen
    :parameters (?animal - animal_type ?meat - meat_type ?location - location_type)
    :task (slaughter ?animal ?meat)
    :precondition ()
    :ordered-subtasks (and
      (look_at ?animal)
      (attack ?animal)
      (gather ?meat))
    )

  (:method m_slaughter_4
    ;; Animal has been seen
    :parameters (?animal - animal_type ?meat - meat_type ?location - location_type)
    :task (slaughter ?animal ?meat)
    :precondition ()
    :ordered-subtasks (and
      (attack ?animal)
      (gather ?meat))
    )

  (:method m_consume_or_obtain_1
    :parameters (?item - item_type)
    :task (consume_or_obtain ?item)
    :precondition ()
    :ordered-subtasks (and
      (gather ?item)
      )
    )

  (:method m_consume_or_obtain_2
    :parameters (?crop - crop_type)
    :task (consume_or_obtain ?crop)
    :precondition (and (not (grows_from ?crop ?crop)))
    :ordered-subtasks (and (grow_and_harvest ?crop))
    )

  (:method m_consume_or_obtain_3
    :parameters (?item - item_type ?animal - animal_type)
    :task (consume_or_obtain ?item)
    :precondition ()
    :ordered-subtasks (and
      (move ?animal)
      (gather ?item)
      )
    )

  (:method m_grow_and_harvest_1
    :parameters (?crop - crop_type ?target - location_type ?seed - non_stem_seeds_type)
    :task (grow_and_harvest ?crop)
    :precondition ()
    :ordered-subtasks (and
      (grow_with_bone_meal ?seed ?target)
      (harvest ?crop)
      (gather ?crop)
      )
    )
    
  (:method m_grow_with_bone_meal_1
    :parameters (
      ?seed - seeds_type
      ?target - location_type)
    :task (grow_with_bone_meal ?seed ?target)
    :precondition ()
    :ordered-subtasks (and
      (move ?target)
      (look_at ?target)
      (select ?seed)
      (use ?seed)
      (select_and_use_bone_meal)
      )
    )

   (:method m_grow_with_bone_meal_2
    :parameters (
      ?seed - seeds_type
      ?target - location_type)
    :task (grow_with_bone_meal ?seed ?target)
    :precondition ()
    :ordered-subtasks (and
      (obtain_growing_items)
      (move ?target)
      (look_at ?target)
      (select ?seed)
      (use ?seed)
      (select_and_use_bone_meal)
      )
    )

 (:method m_select_and_use_bone_meal_1
    :parameters (
      ?bone_meal_1 - bone_meal_type)
    :task (select_and_use_bone_meal)
    :ordered-subtasks (and
      (select ?bone_meal_1)
      (use ?bone_meal_1)
      )
    )

  (:method m_obtain_growing_items_1
    :parameters (?seed - seeds_type)
    :task (obtain_growing_items)
    :ordered-subtasks (and
      (consume_or_obtain ?seed)
      )
    )

  ;; command executive to gather a drop from slaughtering, breaking, harvesting, etc.
  ;; gather is used when an item results from an agent's action (i.e, attack or break)
  (:action gather
    :parameters (?item - item_type)
    :precondition ()
    :effect ()
    )

  ;; command executive to select an item, moving it to the hotbar if neecessary
  (:action select
    :parameters (?new_item - item_type)
    :precondition ()
    :effect ()
    )

  ;; command executive to move near the closest block type
  (:action move
    :parameters (?location - location_type)
    :precondition ()
    :effect ()
    )

  ;; command executive to look at an entity
  (:action look_at
    :parameters (?location - location_type)
    :precondition ()
    :effect ()
    )

  ;; command executive to attack an entity
  (:action attack
    :parameters (?entity - entity_type)
    :precondition ()
    :effect ()
    )

  ; ;; command executive to attack a non-stem crop
  (:action harvest
    :parameters (?crop - crop_type)
    :precondition ()
    :effect ()
    )

  ; ;; command executive to use the currently selected item
  (:action use
    :parameters (?item - item_type)
    :precondition ()
    :effect ()
    )

)
