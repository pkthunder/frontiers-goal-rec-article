"""Interface for Elexir system"""

import subprocess

import cppyy.gbl.std as cpp_std
# import cppyy.ll
from pylexir import pylexir

import numpy as np
#from memory_profiler import profile

# CONFIG_FILE = "configs/elexir-configMCTS-minecraft-high-inspectability.json"
CONFIG_FILE = "configs/elexir-configMCTS.json"

def execute_command(args):
    """Executes command specified by `args` on command line

    Args:
        args (list[str]): Arguments for command. First element
    is the command to execute (i.e. `python`, `cd`, etc.)

    Returns:
        stdout (str): Standard out from executing command
    """

    args = ["/Users/pavankantharaju/Documents/sift/asist/elexir/elexirAppJSON",
            "-e", args[0],
            "-p", "elexir-tmp-prob.json"
            "-o", "output.json"]

    out = subprocess.Popen(args,
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT)
    stdout, _ = out.communicate()
    stdout = stdout.decode("utf-8")
    stdout = stdout.split("\n")[:-1]
    return stdout

# def get_highest_prob_task(task_labels: list, queries: dict, generator: np.random.Generator):
#     """Get highest probability task

#     Args:
#         task_labels (list[str]): List of task labels in domain
#         queries (dict[pylexir.Atom, pylexir.QueryResults])

#     Return:
#         task (str): Highest probability task
#     """

#     highest_prob = -1.0
#     top_level_tasks = list()
#     for _, query_results in queries.items():
#         # _tlt = _tlt_atom.getString()
#         prob = query_results.cp.prob()
#         if prob > highest_prob:
#             highest_prob = prob

#     for task in task_labels:
#         prob = queries[task].cp.prob()
#         print(f"Task: {task}, Prob: {prob}")
#         if prob > 0.0000000000000001 and abs(highest_prob-prob) < 0.0000000000000001:
#             top_level_tasks.append(task)

#     if len(top_level_tasks) == 0:
#         return "none"

#     if len(top_level_tasks) == 1:
#         return top_level_tasks[0]
#     else:
#         index = generator.integers(0, len(top_level_tasks))
#         # index = np.random.randint(0, len(top_level_tasks))
#         return top_level_tasks[index]

class ElexirInterface:
    """Elexir interface

    Args:
        domain_filename (str): Domain file
        seed (int): RNG seed

    Attributes:
        elex_obj (pylexir.Elexir): Pylexir object
        domain_filename (str): Domain filename
        generator (np.random.BitGenerator): Elexir generator
    """
    task_labels = None

    def __init__(self, domain_filename: str, seed: int):
        self.domain_filename = domain_filename
        self.elex_obj = None
        self.generator = np.random.default_rng(seed)
        self.init_elexir()

    def init_elexir(self):
        """Initialize Elexir"""
        # self.elex_obj = pylexir.Elexir(self.domain_filename, 0)
        self.elex_obj = pylexir.Elexir(CONFIG_FILE)
        self.elex_obj.parseFile(self.domain_filename)

    def add_problem_data(self, init_state: list, obs_sequence: list,
                         objects: set, queries: list):
        """Add problem information to Elexir

        Args:
            init_state (list[str]): Initial state
            observations (list[str]): Observations
            objects (set): Objects in problem
            queries (list[str]): Queries

        Returns:
            True if successful. False otherwise.
        """

        for query in queries:
            self.elex_obj.rec.addQuery(query)

        for obj, _type in objects:
            self.elex_obj.declareNewDomainObject(obj, _type)

        for pred in init_state:
            name = pred[0:pred.find("(")]
            params = pred[pred.find("(")+1:pred.find(")")].split(",")
            vector = cpp_std.vector[str](params)
            self.elex_obj.prob.addInitialState(name, vector)

        try:
            for action in obs_sequence:
                name = action[0:action.find("(")]
                params = action[action.find("(")+1:action.find(")")].split(",")
                vector = cpp_std.vector[str](params)
                self.elex_obj.rec.addObservation(name, vector)
        except Exception:
            return False
        return True

    # @profile
    def recognize(self):
        """Run plan recognition"""
        # with cppyy.ll.signals_as_exception():
        self.elex_obj.processProblem()
        # self.elex_obj.saveElexir("elexir-debug.json")

    def get_highest_prob_task(self, task_labels: list, queries: dict):
        """Get highest probability task

        Args:
            task_labels (list[str]): List of task labels in domain
            queries (dict[pylexir.Atom, pylexir.QueryResults])

        Return:
            task (str): Highest probability task
        """

        highest_prob = -1.0
        top_level_tasks = list()
        for _, query_results in queries.items():
            # _tlt = _tlt_atom.getString()
            prob = query_results.cp.prob()
            if prob > highest_prob:
                highest_prob = prob

        for task in task_labels:
            prob = queries[task].cp.prob()
            print(f"Task: {task}, Prob: {prob}")
            if prob > 0.0000000000000001 and abs(highest_prob-prob) < 0.0000000000000001:
                top_level_tasks.append(task)

        if len(top_level_tasks) == 0:
            return "none"

        if len(top_level_tasks) == 1:
            return top_level_tasks[0]

        index = self.generator.integers(0, len(top_level_tasks))
        # index = np.random.randint(0, len(top_level_tasks))
        return top_level_tasks[index]

    def reset(self):
        """Reset Elexir recognition"""
        self.elex_obj.hardReset()
        # self.elex_obj = pylexir.Elexir(self.domain_filename, 0)
        self.elex_obj = pylexir.Elexir(CONFIG_FILE)
        self.elex_obj.parseFile(self.domain_filename)
        # self.elex_obj.loadElexir(self.domain_filename)
